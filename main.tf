data "docker_registry_image" "Prowlarr" {
	name = "linuxserver/prowlarr:nightly"
}

resource "docker_image" "Prowlarr" {
	name = data.docker_registry_image.Prowlarr.name
	pull_triggers = [data.docker_registry_image.Prowlarr.sha256_digest]
}

module "Prowlarr" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.Prowlarr.latest

	networks = [{ name: var.docker_network, aliases: ["prowlarr.${var.internal_domain_base}"] }]
  ports = [{ internal: 9696, external: 9600, protocol: "tcp" }]
	volumes = [
		{
			host_path = "${var.docker_data}/Prowlarr"
			container_path = "/config"
			read_only = false
		}
	]

	environment = {
		"PUID": "${var.uid}",
		"PGID": "${var.gid}",
		"TZ": "${var.tz}"
	}

	stack = var.stack
}